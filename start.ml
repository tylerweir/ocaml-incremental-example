open Core.Std

module Inc = Incremental_lib.Incremental.Make ()
(* Shorthand for making Vars Inc.Vars *)
module Var = Inc.Var

let width_v  = Var.create 3.
let depth_v  = Var.create 5.
let height_v = Var.create 4.

let width    = Var.watch width_v
let depth    = Var.watch depth_v
let height   = Var.watch height_v

let base_area =
  Inc.map2 width depth ~f:( *. )

let volume =
  Inc.map2 base_area height ~f:( *. )

let base_area_obs =
  Inc.observe base_area
let volume_obs    =
  Inc.observe volume

let () =
  let v = Inc.Observer.value_exn in
  let display s =
    printf "%20s: base area %F; volume: %F\n"
      s (v base_area_obs) (v volume_obs)
  in
  Inc.stabilize ();
  display "1st";
  Var.set height_v 10.;
  display "height changed";
  Inc.stabilize ();
  display "2nd"
